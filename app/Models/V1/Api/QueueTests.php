<?php

namespace App\Models\V1\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QueueTests extends Model
{
    use HasFactory;

    protected $table = 'queue_test';

    protected $fillable = [
        'name',
        'info',
    ];

    protected $hidden = [

    ];
}
