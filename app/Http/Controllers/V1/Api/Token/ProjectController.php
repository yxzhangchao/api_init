<?php

namespace App\Http\Controllers\V1\Api\Token;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redis;

class ProjectController extends Controller
{
    //设置
    public function setToken(Request $request)
    {
        /**
         * 1.创建 'refresh_token' 及 'token'
         */

        $token_overdue_time = time() + (3600*2);//token过期时间(两小时)
        $refresh_token_overdue_time = time() + (86400*7);//refresh_token过期时间(7天)

        //1.1：创建token
            $user_name = 'user_1';
            $token = 'user'.$token_overdue_time;
            $refresh_token = 'user_sevenDay'.$refresh_token_overdue_time;

        //1.2:存入Redis中
        Redis::hmset($user_name,'token',$token,'refresh_token',$refresh_token);
    }

    //验证
    public function card(Request $request)
    {
        $header_code =  $request->header('authorization');

        dd($header_code);
    }


    //加密
    public function setCrypt(Request $request)
    {
        $token = Crypt::encryptString('user1632974857');

        return $token;
    }

    //解密
    public function deCrypt(Request $request)
    {
        $header_code =  $request->header('authorization');
        $token_crypt = trim(substr($header_code, (stripos($header_code, "Bearer")+6)));
        
        $decrypted = Crypt::decryptString($token_crypt);

        return $decrypted;
    }
}
