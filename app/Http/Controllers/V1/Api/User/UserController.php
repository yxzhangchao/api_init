<?php

namespace App\Http\Controllers\V1\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    //
    public function userTest(Request $request)
    {

//        $data = $request->session()->all();
//        $value = $request->session()->get('info', 'default');
//        return $data[1];

        return session(1);
    }

    //
    public function read()
    {
        $user_data = User::get();

        return $user_data;
    }

    //
    public function write()
    {
        $data = [
            'name' => 'zc1',
            'phone' => '13227311423',
        ];

        $user_data = User::insert($data);

        return $user_data;
    }

    //
    public function setToken()
    {

    }

    //登录
    public function signIn(Request $request)
    {
        $request_data = $request->all();

        $user_data = User::where([
            ['name','zc'],
            ['phone','18971408102']
        ])->get();

        if (empty($user_data)){
            return response('没有此用户信息');
        }else{

            $info_element = [
                'name' => $request_data['name'],
                'phone' => $request_data['phone']
            ];

            session(['info',$info_element]);
        }

        $data = $request->session()->all();

        return view('signUp/signUp', ['info' => $data[1]]);
    }

}
