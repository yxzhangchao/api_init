<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\V1\Api\InitController;


Route::namespace('V1\Api')->group(function () {

    Route::get('init',[InitController::class, 'init']);
    Route::get('routeTest',[InitController::class,'routeTest']);

    //人员管理
    Route::namespace('User')
        ->prefix('user')
        ->group(base_path('routes/v1/user/api.php'));


    //token管理
    Route::namespace('Token')
        ->prefix('token')
        ->group(base_path('routes/v1/token/api.php'));

    //队列管理（queue）
    Route::namespace('Queue')
        ->prefix('queue')
        ->group(base_path('routes/v1/queue/api.php'));

    //token管理
    Route::namespace('Socket')
        ->prefix('socket')
        ->group(base_path('routes/v1/socket/api.php'));
});

