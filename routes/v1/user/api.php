<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\V1\Api\InitController;

//user测试
Route::get('userTest','UserController@userTest');


Route::get('read','UserController@read');
Route::get('write','UserController@write');


//登录注册测试
Route::middleware('v1/api')->group(function (){
        Route::get('signIn','UserController@signIn');//登录
        //Route::get('write','UserController@write');
    });

