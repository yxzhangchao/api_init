<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\V1\Api\InitController;


Route::get('setToken','ProjectController@setToken');//设置
Route::get('card','ProjectController@card');//验证


Route::get('setCrypt','ProjectController@setCrypt');//加密
Route::get('deCrypt','ProjectController@deCrypt');//解密

